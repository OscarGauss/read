# Math

#### CONCRETE MATHEMATICS
Second Edition

* Ronald L. Graham, AT&T Bell Laboratories
* Donald E. Knuth, Stanford University
* Oren Patashnik, Center for Communications Research

Dedicated to Leonhard Euler (1707{1783)

Link: [Concrete Mathematics, Second Edition.pdf](https://bitbucket.org/OscarGauss/read/src/master/MATH/Concrete%20Mathematics%2C%20Second%20Edition.pdf)

## 